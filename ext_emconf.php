<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'CKEditor Rich Text Editor Image Support',
    'description' => 'Adds FAL image support to CKEditor for TYPO3.',
    'category' => 'be',
    'state' => 'stable',
    'uploadfolder' => 1,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'author' => 'Christian Opitz',
    'author_email' => 'christian.opitz@netresearch.de',
    'version' => '10.1.1',
    'constraints' => [
        'depends' => [
            'typo3' => '10.0.0-10.5.99',
            'rte_ckeditor' => '10.0.0-10.5.99',
        ],
        'conflicts' => [],
        'suggests' => [
            'setup' => '',
        ],
    ],
];
